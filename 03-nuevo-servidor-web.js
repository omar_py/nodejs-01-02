
// incluimos la libreria express
// que debimos haberla instalacion previamente con npm install
const express = require("express");

// creamos una nueva aplicacion express
const app = express();

// iniciamos el servidor en el puerto 3000 indicado por el primer argumento de la funcion
app.listen(3000, () => {
	console.log("Escuchando en el puerto 3000 ...");
});
