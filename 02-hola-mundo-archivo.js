
// importamos una libreria para trabajar con el sistema de archivos
const fs = require('fs');

// escribimos un archivo, con un nombre, un contenido y un mensaje al finalizar
fs.writeFile('salida.txt', 'Hola Mundo', () => {
    console.log('Archivo de salida escrito correctamente.');
});

