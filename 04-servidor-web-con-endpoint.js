// creamos la aplicacion express
const express = require("express");
const app = express();

// definimos un endpoint de tipo GET
app.get("/", (req, res) => {
	console.log("solicitud recibida..");
	res.send("Hola..");
});

// definimos un endpoint de tipo GET
app.get("/bienvenido", (req, res) => {
	console.log("solicitud recibida..");
	res.send("Hola..");
});

// definimos un endpoint de tipo POST
app.post("/crear", (req, res) => {
	console.log("solicitud recibida..");
	res.send("Hola..");
});

// iniciamos el servidor web
app.listen(3000, () => {
	console.log("Escuchando en el puerto 3000 ...");
});
